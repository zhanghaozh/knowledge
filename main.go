package main

import (
	"fmt"
	"knowledge/config"
	"knowledge/dao"
	"knowledge/router"
	"log"
)

func main() {
	conf := config.Config()
	address := conf["address"].String()
	port := conf["port"].String()
	log.Println("正在连接数据库...")
	dao.DB()
	log.Println("连接数据库成功")
	log.Fatal(router.Router.Run(fmt.Sprintf("%s:%s", address, port)))
}
