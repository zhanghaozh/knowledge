package controllers

import (
	"github.com/gin-gonic/gin"
	"knowledge/tools"
)

func Info(ctx *gin.Context) {
	name, _ := ctx.GetQuery("name")
	types, _ := ctx.GetQueryArray("types")
	node := getNode(ctx, name, types)
	tools.Response(ctx, tools.SUCCESS, gin.H{"node": node})
}
